<?php

namespace stlswm\KhyTms\AccessToken;

use stlswm\JsonObject\ClassMap;

/**
 * Class Response
 *
 * @package stlswm\KhyTms\AccessToken
 */
class  Response extends ClassMap
{
    use \stlswm\KhyTms\Response;
    /**
     * @var ResponseData $data
     */
    public $data;

    public function __construct()
    {
        $this->data = new ResponseData();
    }
}
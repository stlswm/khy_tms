<?php

namespace stlswm\KhyTms\AccessToken;

use stlswm\JsonObject\ClassMap;

/**
 * Class ResponseData
 *
 * @package stlswm\KhyTms\AccessToken
 */
class ResponseData extends ClassMap
{
    /**
     * @var string
     */
    public $accessToken = '';
    /**
     * @var string
     */
    public $refreshToken = '';
}
<?php

namespace stlswm\KhyTms\AccessToken;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use stlswm\JsonObject\Json;
use stlswm\KhyTms\Client;

/**
 * Class Request
 *
 * @package stlswm\KhyTms\AccessToken
 */
class Request extends \stlswm\KhyTms\Request
{
    public $userName = '';
    public $password = '';

    /**
     * @return string
     */
    public function jsonFormat(): string
    {
        return json_encode([
            'userName' => $this->userName,
            'password' => $this->password,
        ]);
    }

    /**
     * @param Client $client
     *
     * @return Response
     * @throws GuzzleException
     * @throws Exception
     */
    public function jsonReq(Client $client): Response
    {
        $apiRes = $client->jsonReq('/user/generate_access_token/v2', $this);
        $accessTokenRes = new Response();
        $bool = Json::unMarshal($apiRes, $accessTokenRes);
        if (!$bool) {
            throw new Exception('无法解析返回：' . $apiRes);
        }
        return $accessTokenRes;
    }
}
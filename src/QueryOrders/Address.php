<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Address
 * 收货方地址
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Address extends ClassMap
{
    public $province;
    public $cityCode;
    public $city;
    public $district;
    public $name;//浙江省杭州市淳安县绿城千岛湖喜来登度假酒店
    public $address;//浙江省杭州市淳安县绿城千岛湖喜来登度假酒店
    public $lng;//119.039414
    public $lat;//29.608042
}
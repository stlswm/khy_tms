<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Cargo
 */
class Cargo extends ClassMap
{
    public $id;// //货物id
    public $uid;//创建人id
    public $name;//货物名字，取第一个货物的名字
    public $quantity;//货物总数量
    public $weight;//货物总重量
    public $volume;//货物总体积
    public $productModel;//货物备注
    public $note1;//货物自定义备注1
    public $note2;//货物自定义备注2
    public $note3;//货物自定义备注3
    public $note4;//货物自定义备注4
    public $note5;//货物自定义备注5
    public $note6;//货物自定义备注6
    public $value;//货值
    public $consignerPhone;//发货人手机号
    public $consigneePhones;//收货人手机号
    public $created;//创建货物时间
    public $updated;//更新货物数据时间
}
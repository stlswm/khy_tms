<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class DispatchInfo
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class DispatchInfo extends ClassMap
{
    public $driverPhone;//司机电话
    public $driverUids; //司机列表
    /**
     * @var array $gids
     */
    public $gids;//车队列表
    public $carMode;//车型
    public $orderCount;//生成订单数量
    public $shouldNotifyDriver; //是否给司机发动提醒
    public $needDriverAgree;//是否需要司机同意，默认否(只有一个订单时有效)
    public $driverTags; //司机标签
    public $vehicleTagIds;//车辆标签
}
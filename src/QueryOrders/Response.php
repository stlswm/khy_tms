<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Response
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Response extends ClassMap
{
    use \stlswm\KhyTms\Response;

    /**
     * @var Data $data
     */
    public $data;

    public function __construct()
    {
        $this->data = new Data();
    }
}
<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class ExtraFields
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class ExtraFields extends ClassMap
{
    public $uppay1; // 上游自定义运费1
    public $uppay2; // 上游自定义运费2
    public $uppay3; // 上游自定义运费3
    public $uppay4; // 上游自定义运费4
    public $uppay5; // 上游自定义运费5
    public $uppay6; // 上游自定义运费6
    public $pay1; // 司机自定义运费1
    public $pay2; // 司机自定义运费2
    public $pay3; // 司机自定义运费3
    public $pay4; // 司机自定义运费4
    public $pay5; // 司机自定义运费5
    public $pay6; // 司机自定义运费6
    public $note1; // 自定义备注1
    public $note2; // 自定义备注2
    public $note3; // 自定义备注3
    public $note4; // 自定义备注4
    public $note5; // 自定义备注5
    public $note6; // 自定义备注6
    public $note7; // 自定义备注7
    public $note8; // 自定义备注8
    public $note9; // 自定义备注9
    public $note10; // 自定义备注10
    public $note11; // 自定义备注11
    public $note12; // 自定义备注12
}

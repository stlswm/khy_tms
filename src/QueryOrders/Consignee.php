<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Consignee
 * 收货方信息
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Consignee extends ClassMap
{
    public $name;//收货方
    public $phone;//
    /**
     * @var Address
     */
    public $address;

    public function __construct()
    {
        $this->address = new Address();
    }
}
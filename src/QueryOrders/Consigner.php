<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Consigner
 * 收货方信息
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Consigner extends ClassMap
{
    public $name;//收货方",
    public $phone;//15161128126
    /**
     * @var Address $address
     */
    public $address;

    public function __construct()
    {
        $this->address = new Address();
    }
}
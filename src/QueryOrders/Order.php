<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Order
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Order extends ClassMap
{
    public $id;//订单id
    public $uid;//主账号uid
    public $number;//运单号
    public $wmsNumber;//wmsNumber，即创建订单时的orderNumber
    public $wmsLocation;//集货位
    public $operator;//操作人uid
    public $createUserId;//创建人手机号
    public $createUsername;//创建人姓名
    public $deliveryTime;//提货时间，秒为单位的时间戳
    public $deliveryType;//提送类型 1, "自提"  2, "送货"
    public $appointArriveTime;//预约送达时间，秒为单位的时间戳
    public $customerOrderNumber;//客户单号
    public $startAddress;//起始地址
    public $endAddress;//目的地址
    /**
     * @var Consigner $consigner
     */
    public $consigner;//发货方信息
    /**
     * @var Consignee[] $consignees
     */
    public $consignees;//收货方信息
    /**
     * @var Cargo $cargo
     */
    public $cargo;
    public $totalCargoes;//货物总数量
    public $settlementParty;//结算方
    public $freightIn;//上游运费
    public $freightOut;//司机运费
    public $totalFreightIn;//总上游费用
    public $freightReceived;//已收费用
    public $freightUnreceived;//未收费用
    public $freightCollect;//代收运费
    public $freightCollectReceived;//已收代收运费
    public $freightCollectUnReceived;//未收代收运费
    public $paymentCollect;//代收货款
    public $paymentCollectReceived;//已收代收货款
    public $paymentCollectUnRe;//未收代收货款ceived
    public $needReceipt;//是否需要回单
    public $receiptCount;//回单数量
    public $note;//运单备注
    public $extraFields;
    public $option1;//自定义选项1
    public $option2;//自定义选项2
    public $option3;//自定义选项3
    public $organizationPath;//所属组织机构的path字段(不是名字)，具体请参考常见问题指南
    public $dispatchInfo;//调度信息
    public $batchId;//批量导入的批次id
    public $createUid; //开单人
    public $source;//订单来源，1手工录入，2标准导入，4接口导入
    public $state;//状态  0:"已撤销" 1000:"待处理" 2000:"已发布"
    public $parentOrderId;//父订单id
    public $splitOrderId;//拆分源订单
    public $planNumber;//任务计划的编号
    public $omsNumber;//受理单号
    public $packNumber;//车次号
    public $waybillState;//运单状态，state为2000时才会有值，1已下单，2已接单，3已装货，4已签收
    public $dispatchDriver;//司机信息
    public $evaluate;//评价
    public $miles;//预估里程
    public $openTime;//开单时间
    public $publishTime; //发布时间
    public $receiveTime;//接收时间
    public $shipmentTime;//装货时间
    public $signedTime;//签收时间
    public $receiptCallbackTime;//回单回收时间
    public $receiptSendOutTime;//回单发放时间
    public $evaluateTime;//评价时间
    public $receiptStatus;//回单状态  回单已回收,回单已发放
    public $receiptUploaded;//回单是否已上传
    public $settlementStatus;//运费状态
    public $freightCollectStatus;//代收运费状态 0:未收取 1:已收取 2:已核销 3:部分收取 99:所有
    public $collectionAmountStatus;//代收货款状态 0:未收取 1:已收取 2:已核销 3:部分收取 99:所有
    public $orderSendType;//订单指派方式 1：指派 2：抢单P
    public $orderPrintStatus;//订单打印状态 1：已打印
    public $tipsPrintStatus;//订单标签打印状态 1：已打印
    public $segmentNumber;//分段number
    public $segmentType;//分段类型 0:提货,5:提货到外转方,1:中转,2:直送,3:外转直送,4:外转中继
    public $segmentReceiveOrganizeidPath;//分段收货方组织机构
    public $freezeDeliveryTime;//继承提货时间
    public $freezeAppointArriveTime;//继承预约到达时间
    public $freezeStartAddress;//继承起始地
    public $freezeEndAddress;//继承目的地
    public $freezePaymentCollect;//继承代收货款
    public $abnormal;//异常标记，布尔值，为空则没有异常
    public $abnormal_dispose_status;//异常处理标记，布尔值，为空则没有异常
    public $deliveryAddress;//装货地址
    public $signAddress;//签收地址
    public $onlinePayState;//在线支付状态 1支付中，2支付成功
    public $onlinePayment;//在线支付金额
    public $transactionId;//交易流水号
    public $isDelete;//是否删除 1为删除
    public $created;//订单创建时间
    public $updated;//订单更新时间
    public $startAddressLng;//起点经度
    public $startAddressLat;//起点纬度
    public $endAddressLng;//终点经度
    public $endAddressLat;//终点纬度
    public $financeCollectFixedTime; //代收货款核销时间
    public $freightCollectFixedTime; //代收运费核销时间
    public $apportionFee;//车次分摊费用
    public $checkPickUpAlarm;//提货超时是否检查：1：检查过
    public $checkDeliverAlarm;//送货超时是否检查：1：检查过
    public $onlinePay;//是否在线支付
    /**
     * @var Cargoes[] $cargoes
     */
    public $cargoes;

    public function __construct()
    {
        $this->consigner = new Consigner();
        $this->cargo = new Cargo();
        $this->freightIn = new FreightIn();
        $this->freightOut = new FreightOut();
        $this->extraFields = new ExtraFields();
        $this->dispatchInfo = new DispatchInfo();
        $this->dispatchDriver = new DispatchDriver();
    }

    public function getClassMap(): array
    {
        return [
            'consignees' => Consignee::class,
            'cargoes'    => Cargoes::class,
        ];
    }
}
<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class FreightIn
 * 上游运费
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class FreightIn extends ClassMap
{
    public $payType;//上游费用支付方式， 1现付，2到付，3回付，4周结，5月结
    public $amount;//上游费用
}
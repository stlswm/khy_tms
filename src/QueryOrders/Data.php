<?php


namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Data
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Data extends ClassMap
{
    public $page;
    public $size;
    public $total;
    /**
     * @var Order[] $elements
     */
    public $elements;

    public function getClassMap(): array
    {
        return [
            'elements' => Order::class,
        ];
    }
}
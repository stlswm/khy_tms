<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class Cargoes
 */
class Cargoes extends ClassMap
{
    public $id;
    public $uid;//主账号id
    public $ttmsOrderId;//运单id
    public $name; //货物名称
    public $quantity; //数量
    public $weight; //质量
    public $volume;//体积
    public $value; //货物价值
    public $productModel; //备注
    public $note1;//自定义备注1
    public $note2;//自定义备注2
    public $note3; //自定义备注3
    public $note4; //自定义备注4
    public $note5;//自定义备注5
    public $note6; //自定义备注6
    public $created;//创建时间
}
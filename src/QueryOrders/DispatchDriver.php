<?php

namespace stlswm\KhyTms\QueryOrders;

use stlswm\JsonObject\ClassMap;

/**
 * Class DispatchDriver
 * 司机信息
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class DispatchDriver extends ClassMap
{
    public $uid;// //司机账号的id
    public $username;//名字
    public $phoneNumber; //电话
    public $carNumber;//车牌号
    public $carMode;//车型
    public $carDetailMode;//详细车型
    public $passType;//通行证类型 0:未指定 1:邮政通 2:绿通 3:全市通 4:三环通 5:江南通 6:江北通
    public $passTypeValue;//通行证
}
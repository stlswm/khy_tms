<?php

namespace stlswm\KhyTms\ModifyOrder;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use stlswm\JsonObject\Json;
use stlswm\KhyTms\Client;

/**
 * Class Request
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Request extends \stlswm\KhyTms\Request
{
    public $orderNumber;//订单号
    public $order;//修改运单参数

    public function jsonFormat(): string
    {
        return json_encode([
            'orderNumber' => $this->orderNumber,
            'order'       => $this->order,
        ]);
    }

    /**
     * @param Client $client
     *
     * @return Response
     * @throws GuzzleException
     * @throws Exception
     */
    public function jsonReq(Client $client): Response
    {
        $apiRes = $client->jsonReq('/order/modify_order', $this);
        $response = new Response();
        $bool = Json::unMarshal($apiRes, $response);
        if (!$bool) {
            throw new Exception('无法解析返回：' . $apiRes);
        }
        return $response;
    }
}
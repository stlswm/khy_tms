<?php

namespace stlswm\KhyTms\ModifyOrder;

use stlswm\JsonObject\ClassMap;

/**
 * Class Response
 *
 * @package stlswm\KhyTms\ModifyOrder
 */
class Response extends ClassMap
{
    use \stlswm\KhyTms\Response;
    public $data;
}
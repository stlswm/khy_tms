<?php

namespace stlswm\KhyTms;

/**
 * Class Request
 * 请求对象抽象类
 *
 * @package stlswm\KhyTms
 */
abstract class Request
{
    abstract public function jsonFormat(): string;
}
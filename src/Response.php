<?php

namespace stlswm\KhyTms;

/**
 * Class Response
 * 返回对象
 *
 * @package stlswm\KhyTms
 */
trait Response
{
    /**
     * @var int 返回状态码
     */
    public $code;

    /**
     * @var null|string 返回消息
     */
    public $message = null;
}
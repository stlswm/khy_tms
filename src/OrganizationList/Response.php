<?php

namespace stlswm\KhyTms\OrganizationList;

use stlswm\JsonObject\ClassMap;

/**
 * Class Response
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Response extends ClassMap
{
    use \stlswm\KhyTms\Response;

    /**
     * @var Organization[] $data
     */
    public $data;

    /**
     * @return array
     */
    public function getClassMap(): array
    {
        return [
            'data' => Organization::class,
        ];
    }
}
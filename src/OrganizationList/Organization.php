<?php

namespace stlswm\KhyTms\OrganizationList;

use stlswm\JsonObject\ClassMap;

/**
 * Class Organization
 *
 * @package stlswm\KhyTms\OrganizationList
 */
class Organization extends ClassMap
{
    public $nodeId;// 机构节点id
    public $name;// 机构名
    public $path;// 父节点路径
}
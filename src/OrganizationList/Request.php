<?php

namespace stlswm\KhyTms\OrganizationList;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use stlswm\JsonObject\Json;
use stlswm\KhyTms\Client;

/**
 * Class Request
 *
 * @package stlswm\KhyTms\QueryOrders
 */
class Request extends \stlswm\KhyTms\Request
{

    public function jsonFormat(): string
    {
        return '';
    }

    /**
     * @param Client $client
     *
     * @return Response
     * @throws GuzzleException
     * @throws Exception
     */
    public function jsonReq(Client $client): Response
    {
        $apiRes = $client->jsonReq('/organization/list', $this);
        $response = new Response();
        $bool = Json::unMarshal($apiRes, $response);
        if (!$bool) {
            throw new Exception('无法解析返回：' . $apiRes);
        }
        return $response;
    }
}
<?php

use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use stlswm\KhyTms\Client;
use stlswm\KhyTms\ModifyOrder\Request;

require_once '../vendor/autoload.php';

/**
 * Class AccessTokenTest
 */
class ModifyOrderTest extends TestCase
{
    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function testReq()
    {
        $client = Client::newClient('19012345678', '123456');
        $client->asTest();
        $client->setAccessToken('0e815a39-a445-4ff1-83bc-b427dd8ab6df');
        $req = new Request();
        $req->orderNumber = '1911051350220260';
        $req->order = [
            'organizationPath' => '0-11',
        ];
        $res = $req->jsonReq($client);
        var_dump($res);
    }
}
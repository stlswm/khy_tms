<?php

use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use stlswm\KhyTms\AccessToken\Request;
use stlswm\KhyTms\Client;

require_once '../vendor/autoload.php';

/**
 * Class AccessTokenTest
 */
class AccessTokenTest extends TestCase
{
    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function testReq()
    {
        $client = Client::newClient('19012345678', '123456');
        $client->asTest();
        $req = new Request();
        $req->userName = '19012345678';
        $req->password = '123456';
        $res = $req->jsonReq($client);
        var_dump($res);
    }
}
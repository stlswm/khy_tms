<?php

use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use stlswm\KhyTms\Client;
use stlswm\KhyTms\OrganizationList\Request;

require_once '../vendor/autoload.php';

/**
 * Class OrganizationListTest
 */
class OrganizationListTest extends TestCase
{
    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function testReq()
    {
        $client = Client::newClient('19012345678', '123456');
        $client->asTest();
        $client->setAccessToken('0e815a39-a445-4ff1-83bc-b427dd8ab6df');
        $req = new Request();
        $res = $req->jsonReq($client);
        var_dump($res);
    }
}